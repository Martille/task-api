from datetime import datetime

from connexion import NoContent

tasks = {}


class Error(object):

    def __init__(self, code, message):
        self.code = code
        self.message = message


def get_all():
    """
    Get all the tasks, with no filter
    :return: all tasks
    """
    return list(tasks.values())


def create(task):
    """
    Create a task with given name, priority and description
    :param task: given values for the new task
    :return: the task newly created, with the creation_date and id completed
    """
    max_id = get_max_id()
    task['id'] = max_id + 1
    task['creation_date'] = datetime.now()
    task['last_update'] = None
    tasks[task['id']] = task
    return task, 200


def get(task_id):
    """
    Get the task corresponding to the given id
    :param task_id: the id we're looking for
    :return: the task if found, NoContent if not
    """
    task = tasks.get(task_id)
    if task is not None:
        return task, 200
    else:
        return NoContent, 404


def update(task_id, task):
    """
    Update the task with the given id. If no task exists with this id, it will create one (with the given id)
    :param task_id: id of the task we're want to edit
    :param task: new values of the task (name, description, priority). If one value is missing or empty, this new value will be empty
    :return: the task edited (or the one created if the id didn't exist)
    """
    task_to_update = tasks.get(task_id)

    if task_to_update is not None:
        task_to_update['name'] = task['name']
        task_to_update['description'] = task['description']
        task_to_update['priority'] = task['priority']
        task_to_update['last_update'] = datetime.now()

        tasks[task_id] = task_to_update
        return task_to_update, 200
    else:
        task['id'] = task_id
        task['creation_date'] = datetime.now()
        task['last_update'] = None
        tasks[task['id']] = task
        return task, 201


def delete(task_id):
    """
    Delete a task, given the id
    :param task_id: id of the task we want to delete
    :return: 202 if the task is found and deleted, 404 if no task has this id
    """
    task_to_delete = tasks[task_id]

    if task_to_delete is None:
        return NoContent, 404
    else:
        del tasks[task_id]
        return NoContent, 204


def search(min_date=None, max_date=None, priority_at_least=None, keyword=None):
    """
    Search the tasks that corresponds to the given parameters.
    :param min_date: the earliest date from which the search will filter on the creation_date. Format : YYYY-MM-DD hh:mm:ss (time is not mandatory, separator between date and time can be any character)
    :param max_date: the latest date from which the search will filter on the creation_date. Format : YYYY-MM-DD hh:mm:ss (time is not mandatory, separator between date and time can be any character)
    :param priority_at_least: the minimal priority of the task
    :param keyword: a keyworkd to find in the name or description
    :return: tasks that conforms to ALL the parameters
    """
    filtered = list(tasks.values())

    if min_date is not None:
        if type(min_date) is not datetime:
            try:
                min_date = datetime.fromisoformat(min_date)
            except ValueError:
                print("toto")
                return Error(code="400", message="Wrong min date format. Format is YYYY-MM-DD hh:mm:ss (time is not mandatory, and separator between date and time can be any character)").__dict__, 400
        filtered = filter(lambda t: t['creation_date'] >= min_date, filtered)
    if max_date is not None:
        if type(max_date) is not datetime:
            try:
                max_date = datetime.fromisoformat(max_date)
            except ValueError:
                print("toto")
                return Error(code="400", message="Wrong max date format. Format is YYYY-MM-DD hh:mm:ss (time is not mandatory, and separator between date and time can be any character)").__dict__, 400
        filtered = filter(lambda t: t['creation_date'] <= max_date, filtered)

    if priority_at_least is not None:
        filtered = filter(lambda t: t['priority'] >= priority_at_least, filtered)

    if keyword is not None:
        filtered = filter(lambda t: keyword in t['name'].lower() or keyword in t['description'].lower(), filtered)

    return list(filtered), 200


def get_max_id():
    if len(tasks) == 0:
        return 0
    else:
        return max(list(tasks))
