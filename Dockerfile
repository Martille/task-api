FROM python:3.9

WORKDIR task-api

COPY api/ api/
COPY main.py .
COPY requirements.txt .
COPY Makefile .

RUN make requirements

CMD make run