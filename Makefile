
requirements:
	echo "make requirements"
	pip install -r requirements.txt

run:
	python ./main.py

tests:
	python -m unittest test.test_task
