from datetime import datetime
from unittest import TestCase

from api import task


def empty_tasks():
    task.tasks = {}


class TaskTest(TestCase):
    def test_create(self):
        # Given
        empty_tasks()
        task_to_create = {'name': 'Nom tâche', 'priority': 42, 'description': 'ma description'}

        # When
        task_created, return_code = task.create(task_to_create)

        # Then
        self.assertEqual(len(task.get_all()), 1)

        self.assertIsNotNone(task_created['id'])
        self.assertIsNotNone(task_created['creation_date'])
        self.assertIsNone(task_created['last_update'])
        self.assertEqual(task_created['name'], task_to_create['name'])
        self.assertEqual(task_created['priority'], task_to_create['priority'])
        self.assertEqual(task_created['description'], task_to_create['description'])

    def test_get_with_known_id(self):
        # Given
        empty_tasks()
        task_created, _ = task.create({'name': 'Nom tâche', 'priority': 42, 'description': 'ma description'})

        # When
        my_task, return_code = task.get(task_created['id'])

        # Then
        self.assertEqual(len(task.get_all()), 1)
        self.assertIsNotNone(my_task)
        self.assertEqual(return_code, 200)

    def test_get_with_unknown_id(self):
        # Given
        empty_tasks()
        task.create({'name': 'Nom tâche', 'priority': 42, 'description': 'ma description'})

        # When
        my_task, return_code = task.get(100)

        # Then
        self.assertEqual(return_code, 404)

    def test_update_with_known_id(self):
        # Given
        empty_tasks()
        task_created, _ = task.create({'name': 'Nom tâche', 'priority': 42, 'description': 'ma description'})
        task_new_values = {'name': 'Nom tâche mis à jour',
                           'priority': 420,
                           'description': 'Ma nouvelle description'}

        # When
        task_updated, return_code = task.update(task_created['id'], task_new_values)

        # Then
        self.assertEqual(len(task.get_all()), 1)
        self.assertIsNotNone(task_updated)
        self.assertEqual(return_code, 200)

        self.assertIsNotNone(task_updated['id'])
        self.assertIsNotNone(task_updated['creation_date'])
        self.assertIsNotNone(task_updated['last_update'])
        self.assertEqual(task_updated['name'], task_new_values['name'])
        self.assertEqual(task_updated['priority'], task_new_values['priority'])
        self.assertEqual(task_updated['description'], task_new_values['description'])

    def test_update_with_unknown_id(self):
        # Given
        empty_tasks()
        task_created = task.create({'name': 'Nom tâche', 'priority': 42, 'description': 'ma description'})
        task_new_values = {'name': 'Nom tâche mis à jour',
                           'priority': 420,
                           'description': 'Ma nouvelle description'}

        # When
        task_created, return_code = task.update(100, task_new_values)

        # Then
        self.assertEqual(len(task.get_all()), 2)
        self.assertIsNotNone(task_created)

        self.assertIsNotNone(task_created['id'])
        self.assertIsNotNone(task_created['creation_date'])
        self.assertIsNone(task_created['last_update'])
        self.assertEqual(task_created['name'], task_new_values['name'])
        self.assertEqual(task_created['priority'], task_new_values['priority'])
        self.assertEqual(task_created['description'], task_new_values['description'])

    def test_delete(self):
        # Given
        empty_tasks()
        task_created, _ = task.create({'name': 'Nom tâche', 'priority': 42, 'description': 'ma description'})

        # When
        _, return_code_delete = task.delete(task_created['id'])

        # Then
        self.assertEqual(return_code_delete, 204)
        expecting_nothing, return_code = task.get(task_created['id'])
        self.assertEqual(return_code, 404)

    def test_search_by_date_with_only_min_date(self):
        # Given
        init_taks()

        # When
        result, return_code = task.search(min_date="2020-10-25T10:00:00")

        # Then
        self.assertEqual(len(result), 2)
        list_ids = list(map(lambda t: t['id'], result))
        self.assertIn(1, list_ids)
        self.assertIn(3, list_ids)

    def test_search_by_date_with_only_max_date(self):
        # Given
        init_taks()

        # When
        result, return_code = task.search(max_date="2020-11-13T10:00:00")

        # Then
        self.assertEqual(len(result), 2)
        list_ids = list(map(lambda t: t['id'], result))
        self.assertIn(2, list_ids)
        self.assertIn(3, list_ids)

    def test_search_by_date_with_min_and_max_date(self):
        # Given
        init_taks()

        # When
        result, return_code = task.search(min_date="2020-10-25T10:00:00", max_date="2020-11-13T10:00:00")

        # Then
        self.assertEqual(len(result), 1)
        list_ids = list(map(lambda t: t['id'], result))
        self.assertIn(3, list_ids)

    def test_search_by_date_with_priority(self):
        # Given
        init_taks()

        # When
        result, return_code = task.search(priority_at_least=150)

        # Then
        self.assertEqual(len(result), 2)
        list_ids = list(map(lambda t: t['id'], result))
        self.assertIn(1, list_ids)
        self.assertIn(3, list_ids)

    def test_search_by_date_with_keyword(self):
        # Given
        init_taks()

        # When
        result, return_code = task.search(keyword="breakfast")

        # Then
        self.assertEqual(len(result), 1)
        list_ids = list(map(lambda t: t['id'], result))
        self.assertIn(2, list_ids)

    def test_search_by_date_with_several_parameters(self):
        # Given
        init_taks()

        # When
        result, return_code = task.search(min_date="2020-10-20T10:00:00", keyword="witch")

        # Then
        self.assertEqual(len(result), 1)
        list_ids = list(map(lambda t: t['id'], result))
        self.assertIn(3, list_ids)


def init_taks():
    empty_tasks()

    # We use update to choose the id of tasks (not supposed to be used like this, it's only for tests)
    task1_id = 1
    task1, _ = task.update(task1_id, {'name': 'Destroy the ring',
                                      'priority': 1000,
                                      'description': 'Put the ring into mount doom'})
    task.tasks[task1_id]['creation_date'] = datetime.fromisoformat("2020-11-15T14:30:00")

    task2_id = 2
    task2, _ = task.update(task2_id, {'name': 'Have a second breakfast',
                                      'priority': 12,
                                      'description': 'Cook rabbit stew, not for the witch king'})
    task.tasks[task2_id]['creation_date'] = datetime.fromisoformat("2020-10-14T10:00:00")

    task3_id = 3
    task3, _ = task.update(task3_id, {'name': 'Kill the witch king',
                                      'priority': 150,
                                      'description': 'I am no man'})
    task.tasks[task3_id]['creation_date'] = datetime.fromisoformat("2020-10-27T18:45:00")
