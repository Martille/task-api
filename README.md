# Task API

Création d'une API CRUD en python 3 permettant la gestion de tâches.
> Swagger dispo : http://localhost:8080/ui

Framework utilisé : connexion (voir requirements.txt)

### Docker
    docker build -t task-api .
    docker run --rm -p 8080:8080 --name task-api task-api
    
L'API est protégée avec un Token (pour le moment en dur dans l'application). Dans le header, ajouter :
> X-API-Key : key-1234567890

## Fonctionnalités implémentées

* Modèle de l'objet tâche contenant les champs :
    * id (permet d'assurer l'unicité)
    * name
    * creation_date
    * last_update
    * priority
    * description
    
* Stockage en RAM des objets
    
* Services (basic CRUD) :
    - Basic CRUD : Création, Edition, Suppression (by id), Get by id, get all
    - Recherche :
        * avec filtrage temporels
        * avec filtrage par priorité
        * par mot clef

* Protection de l'api avec une vérification de token
        
* Packaging du livrable
    * Ecrire un Dockerfile pour le projet
    * Avoir un makefile permettant:
        * L'installation des dépendances : make requirements
        * L'execution de l'application : make run
        * L'execution des tests unitaires : make test
    * Execution des tests avec gitlab-ci

## TODO

* Voir comment passer d'un dict à un objet pour le modèle (problème de serialization json)
* Plus de tests ?
* Améliorer le Makefile

* Bonus:
    * Implémenter un système de caching pour les requêtes (lru, redis,...)
