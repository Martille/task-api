import connexion

if __name__ == '__main__':
    app = connexion.FlaskApp(__name__, port=8080, specification_dir='api/')
    app.add_api('task_api.yml')
    app.run()
